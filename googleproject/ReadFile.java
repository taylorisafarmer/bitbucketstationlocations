//Edited by Taylor Farmer
//@author Gary Lewandowski
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class ReadFile
{
    //Create ArrayList - myFile for words to be stored in
    ArrayList<String> myFile = new ArrayList<String>();

    //echo given file
    public void readIt(Scanner infile)
    {
        //Go through each word and add them to ArrayList - myFile
        while (infile.hasNext())
        {
            String word = infile.next();
            myFile.add(word);
        }
        /*Print ArrayList - myFile
        *for(int i=0 ; i < myFile.size() ; i++)
        *    {
        *        System.out.println("City : " + myFile.get(i));
        *    }
        */
    }

    //method for GoogleTest to get size of myFile(arraylist)
    public int getSize()
    {
        return myFile.size();
    }

    //used by GoogleTest to get the string from an index of myFile
    public String getArrayIndex(int i)
    {
        return myFile.get(i);
    }

    public static void main(String [] args)
    {
        File fileToRead = new File("testtext.txt");
        try
        {
            Scanner in = new Scanner(fileToRead);
            ReadFile mainObject = new ReadFile();
            mainObject.readIt(in);
        }
        catch(IOException e)
        {
            System.out.println("Stuff went wrong! "+ e);
        }
    }
}