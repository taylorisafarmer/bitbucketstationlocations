/** main.cpp
* Taylor Farmer
* Runs Huffman stuff
**/
#include <iostream>
#include <string>
#include <vector>
#include "HuffmanNode.h"
#include "Count.h"
using namespace std;
int main()
{
  string file = "importedFile.txt";
	Count startingThings;
  vector<string> paththingy = startingThings.getPath();
  HuffmanNode* something = startingThings.countFile(file).Value();
  startingThings.traverse(something, "", paththingy);
  startingThings.showMeTheMoney(file, paththingy);
}
