//heapnode.h
//Taylor Farmer
#ifndef HEAPNODE_H
#define HEAPNODE_H
#include "huffmanNode.h"
#include <iostream>
#include <vector>
#include <queue>
#include <string>
using namespace std;

class HeapNode
{
  private:
    HuffmanNode *huffmanNodePointer;

  public:
    bool operator<(const HeapNode &rightHandArgument) const;
    HuffmanNode* Value();
    void buildLeaf(int w, int v);
    void buildNode(HuffmanNode *leftChild, HuffmanNode *rightChild);
};
#endif
