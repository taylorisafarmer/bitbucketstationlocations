//Count.h
//Taylor Farmer
#ifndef COUNT_H
#define COUNT_H
#include <vector>
#include <fstream>
#include <queue>
#include <string>
#include "HeapNode.h"
#include "HuffmanNode.h"
using namespace std;

class Count
{
private:
  static const int PSEUDOEOF = 256;
  std::vector<int> charCount; // 257 entries, each set to 0
  string filename;
public:
  Count();
  priority_queue<HeapNode> theHeap;
  HeapNode countFile(string importedFile);
  void traverse(HuffmanNode* node, string path, vector<string>& thePath);
  HeapNode buildTree();
  vector<string> thePath;
  void showMeTheMoney(string importedFile, vector<string> thePath);
  vector<string> getPath();
};
#endif
