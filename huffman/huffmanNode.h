//huffmanNode.h
//Taylor Farmer
#ifndef HUFFMANNODE_H
#define HUFFMANNODE_H

class HuffmanNode
{
private:
  int weight;
  int value;
  HuffmanNode *rightChild;
  HuffmanNode *leftChild;

public:
  HuffmanNode(int w, int v);
  HuffmanNode(HuffmanNode *lefty, HuffmanNode *righty);
  int Weight();
  int Value();
  HuffmanNode* Left();
  HuffmanNode* Right();

};
#endif
