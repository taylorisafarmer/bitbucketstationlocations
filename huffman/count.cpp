//Count.cpp
//Taylor Farmer
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "Count.h"
#include "HeapNode.h"
#include "HuffmanNode.h"
using namespace std;

Count::Count()
{
  charCount.resize(257,0);
  thePath.resize(257);
}

HeapNode Count::countFile(string importedFile) // reading the file
{
  filename = importedFile;
  ifstream infile;
  infile.open(filename.c_str());

//while peeking ahead does not reveal end of file
  while(infile.peek() && !infile.eof())
  {
    char ch = infile.get();
    charCount[ch]++;
  }

  HeapNode nodie; //New heapNode to mess with
  for(int i = 0; i < charCount.size(); i++) //makes heapNode with weight and value of each character
  {                                         //makes leafs
    if(charCount[i] > 0)
    {
      nodie.buildLeaf(charCount[i], i);
      theHeap.push(nodie);
    }
  }

  while(theHeap.size() > 1) //crack the nodies open and place them on the tree
  {
    HeapNode bigNodie; //New huffmanNodes: I put it in the loop so it's not a dirty bigNodie
    HeapNode huff1 = theHeap.top();
    theHeap.pop();
    HeapNode huff2 = theHeap.top();
    theHeap.pop();
    bigNodie.buildNode(huff1.Value(), huff2.Value());
    theHeap.push(bigNodie);
  }
  return theHeap.top();
}

void Count::traverse(HuffmanNode* node, string path, vector<string>& thePath) //traverses the tree and gives you paths
  {
    if(node == NULL)
      return;
    if(node->Left() != NULL)
      traverse (node->Left(), path + "0", thePath);
    if(node->Value() != -1)
      thePath[(char) node->Value()] = path;
      cout << "I've gotten there: " << (char) node->Value() << " | " << path << endl;
    if(node->Right() != NULL)
      traverse (node->Right(), path + "1", thePath);
  }

void Count::showMeTheMoney(string importedFile, vector<string> thePath) // prints out the binary string
{
  filename = importedFile;
  ifstream infile;
  infile.open(filename.c_str());

//while peeking ahead does not reveal end of file
  while(infile.peek() && !infile.eof())
  {
    char ch = infile.get();
    cout << thePath[ch];
  }
  cout << endl;
}

vector<string> Count::getPath()
{
  return thePath;
}
