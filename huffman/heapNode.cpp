//heapnode.cpp
//Taylor Farmer
#include <iostream>
#include "HeapNode.h"
#include <vector>
#include <string>
#include <fstream>
#include <queue>
#include "HuffmanNode.h"
using namespace std;

bool HeapNode::operator<(const HeapNode &rightHandArgument) const //changed what < does
{
  return huffmanNodePointer->Weight()>rightHandArgument.huffmanNodePointer->Weight();
}

void HeapNode::buildLeaf(int w, int v) //build a leaf that is the end of the tree thingy
{
  huffmanNodePointer = new HuffmanNode(w, v);
}

void HeapNode::buildNode(HuffmanNode *leftChild, HuffmanNode *rightChild) //build a node made of leafs
{
  huffmanNodePointer = new HuffmanNode(leftChild, rightChild);
}

HuffmanNode* HeapNode::Value()
{
  return huffmanNodePointer;
}
