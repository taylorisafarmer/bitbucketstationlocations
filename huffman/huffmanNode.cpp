//huffmanNode.cpp
//Taylor Farmer
#include <iostream>
#include "huffmanNode.h"
#include <vector>
#include <string>
#include <fstream>
using namespace std;

HuffmanNode::HuffmanNode(int w, int v) //constructor for leafs
{
  weight = w;
  value = v;
  leftChild = NULL;
  rightChild = NULL;
}

HuffmanNode::HuffmanNode(HuffmanNode *lefty, HuffmanNode *righty) //constructor for nodes
{
  leftChild = lefty;
  rightChild = righty;
  value = -1;
  weight = leftChild->Weight()+rightChild->Weight();
}

int HuffmanNode::Weight()
{
  return weight;
}

int HuffmanNode::Value()
{
  return value;
}

HuffmanNode* HuffmanNode::Left()
{
  return leftChild;
}

HuffmanNode* HuffmanNode::Right()
{
  return rightChild;
}
