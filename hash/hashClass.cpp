//hashClass.cpp
//Taylor Farmer
//reads in a file

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include "hashtab.h"
#include "hashClass.h"
#include "hashFile.h"
using namespace std;

hashval_t HashClass::hashMe(string importedString)
{
  string s = importedString;
  hashval_t h = 0;
  for(int i = 0; i<s.size(); i++)
  {
    h = h^magicbits[s[i]][i];
  }
  return h;
}
