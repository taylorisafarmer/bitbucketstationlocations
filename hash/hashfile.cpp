//hashfile.cpp
//Taylor Farmer
//reads in a file

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <cstddef>
#include <map>
#include "hashFile.h"
#include "hashClass.h"
#include "hashtab.h"
using namespace std;

void HashFile::readFile(string importedFile, string secondImportedFile)
{
  windowtothewall.resize(3); // Window vector
  filename = importedFile;
  //secondWindowtothewall.resize(3); // second window vector
  //secondFilename = secondImportedFile;
  ifstream infile;
  infile.open(filename.c_str());
  //infile.open(secondFilename.c_str());

  string tempString = ""; // temp string to be used to store line that needs to be hashed
  int i = 0; //current line
  int a = 0; //iterator for hashed vector
  int line = 0; // keeps track of which of the 3 lines in the window you are on
  while(!infile.eof()) //loops till end of file
  {
    if (i>2) //if on the third line of the window, set i back to 0
    {
      i = 0;
    }
    getline(infile, tempString);
    windowtothewall[i] = tempString;
    //hash entire 3 lines
    if (i == 2) //hash the window and print out the line and hash
    {
      string tempstuff = windowtothewall[0] + windowtothewall[1] + windowtothewall[2];
      windowtothewall2.push_back(hasher.hashMe(tempstuff));
      m.insert ( pair<hashval_t, int>(windowtothewall2[a], line) );
      cout << "line: " << line << " | " << windowtothewall2[a] << endl;
      a++;
      line++;
      i = i - 2;
    }
    i++;
  }
}
