//hashClass.h
//Taylor Farmer
//reads in a file
#ifndef HASHCLASS_H
#define HASHCLASS_H

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
using namespace std;

class HashClass
{
public:
  hashval_t hashMe(string importedString);
};
#endif
