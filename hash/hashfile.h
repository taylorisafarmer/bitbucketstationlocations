//hashfile.h
//Taylor Farmer
//reads in a file

#ifndef HASHFILE_H
#define HASHFILE_H

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include "hashtab.h"
#include "hashClass.h"
using namespace std;

class HashFile
{
private:
  std::vector<string> windowtothewall;
  std::vector<hashval_t> windowtothewall2;
  std::vector<int> lineVector;
  std::map<hashval_t, int> m;
  string filename;

  std::vector<string> secondWindowtothewall;
  std::vector<hashval_t> secondWindowtothewall2;
  std::vector<int> secondlineVector;
  std::map<hashval_t, int> secondM;
  string secondFilename;

  HashClass hasher;
  void hashMethod();
public:
  void readFile(string importedString, string secondImportedString);
};
#endif
