//Game.h
//Taylor Farmer

#ifndef GAME_H
#define GAME_H
#include "Parser.h"
#include "Room.h"
#include "Command.h"
#include "CommandWords.h"

class Game
{
  private:
    Parser* parser;
    Room* currentRoom;
    void createRooms();
    void printWelcome();
    bool processCommand(Command command);
    void printHelp();
    void goRoom(Command command);
    bool quit(Command command);

  public:
    void play();
    Game();
};
#endif
