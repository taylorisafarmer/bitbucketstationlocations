//Parser.h
//Taylor Farmer

#ifndef PARSER_H
#define PARSER_H
#include "CommandWords.h"
#include "Command.h"
using namespace std;

class Parser
{
    private:
      CommandWords commands;  // holds all valid command words

    public:
       Parser();
       Command getCommand();
       void showCommands();
};

#endif
