//Room.h
//Taylor Farmer

#ifndef ROOM_H
#define ROOM_H
#include<map>
#include<string>
using namespace std;

class Room
{
    private:
      string description;
      std::map<string,Room*> exits;

    public:
       Room(string description);
       void setExit(string direction, Room* neighbor);
       string getShortDescription();
       string getLongDescription();
       string getExitString();
       Room* getExit(string direction);
};
#endif
