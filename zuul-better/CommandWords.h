//CommandWords.h
//Taylor Farmer

#ifndef COMMANDWORDS_H
#define COMMANDWORDS_H
#include<string>
#include<vector>
using namespace std;

class CommandWords
{
  private:
    std::vector<string> validCommands;
  public:
    CommandWords();
    bool isCommand(string aString);
    void showAll();
};
#endif
