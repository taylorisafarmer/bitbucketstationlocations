//CommandWords.cpp
//Taylor Farmer

/**
 * This class is part of the "World of Zuul" application.
 * "World of Zuul" is a very simple, text based adventure game.
 *
 * This class holds an enumeration of all command words known to the game.
 * It is used to recognise commands as they are typed in.
 *
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.08.08
 */

 #include "CommandWords.h"
 #include<string>
 #include<iostream>
 #include<vector>
 #include<algorithm>
 using namespace std;

    /**
     * Constructor - initialise the command words.
     */
    CommandWords::CommandWords()
    {
      validCommands.push_back ("go");
      validCommands.push_back ("quit");
      validCommands.push_back ("help");
    }

    /**
     * Check whether a given String is a valid command word.
     * @return true if it is, false if it isn't.
     */
    bool CommandWords::isCommand(string aString)
    {
      bool found = (std::find(validCommands.begin(), validCommands.end(), aString) != validCommands.end());
      return found;
    }

    /**
     * Print all valid commands to System.out.
     */
     void CommandWords::showAll()
    {

      for(vector<string>::iterator command = validCommands.begin();
      command != validCommands.end(); command++)
      {
        cout << (*command) << "  " << endl;
      }
        cout << "" << endl;
    }
